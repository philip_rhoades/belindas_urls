#!/bin/sh

rm Gemfile Gemfile.lock _config.yml

ln -s ./Gemfile.gl ./Gemfile
ln -s ./Gemfile.lock.gl ./Gemfile.lock
ln -s ./_config.yml.gl ./_config.yml

