#!/bin/sh

while read url
do
	if [[ "$url" == *"youtube"* ]]; then
#		echo $url
		tmp=${url#*?v=}
#		echo $tmp
#		CutyCapt --url=https://img.youtube.com/vi/$tmp/0.jpg --delay=10000 --out=t.png
		echo -n "[<img src=\""
		echo -n "https://img.youtube.com/vi/$tmp/0.jpg"
		echo -n "\">]"
		echo -n "("
		echo -n $url
		echo ")"
		echo ""
#		convert t.png -trim $tmp.png
	else
		tmp=${url#http*\/\/}
		img=`echo $tmp | tr "/" "_"`
		CutyCapt --url=$url --delay=10000 --out=../images/$img.png
		echo -n "[<img src=\""
		echo -n "{{site.url}}/images/$img.png"
		echo -n "\">]"
		echo -n "("
		echo -n $url
		echo ")"
		echo ""
#		echo $url
	fi
done < ../urls$1.txt





