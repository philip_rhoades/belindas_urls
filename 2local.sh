#!/bin/sh

rm Gemfile Gemfile.lock _config.yml

ln -s ./Gemfile.f31 ./Gemfile
ln -s ./Gemfile.lock.f31 ./Gemfile.lock
ln -s ./_config.yml.f31 ./_config.yml

